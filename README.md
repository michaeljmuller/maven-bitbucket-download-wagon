A Maven component (a wagon) that enables deploying artifacts in Downloads section of a particular Bitbucket.org repository.

This wagon **does not store your artifacts in git** - it uses download section of each repository, so no strange links, no additional git setup and temporary files anywhere.

# Who should use it and who should not?

You can freely convert Downloads sections of bitbucket.org by using this maven extension, if:

* You don't want to setup or subscribe additional repository management software
* You want to have a simple solution
* You want to keep your distribution and artifact files close to the source
* You want to keep your dependencies for project close to the project sources
* You would like to hand on the responsibilities over project distributions to its maintainers - not administration of another tools
* You like bitbucket pipelines taking away the job from other CI tools and would like to go further with relying only on bitbucket :)

Don't you use the maven bitbucket downloads wagon, if:

* You don't like to support the tools you use. This wagon is a fresh tool, rough on the edges :)
* You must obey strong corporate access policies - this is a responsibilities of greater tools like artifactory or nexus
* You want to have a transitive repository or mirror features

# Features

* Can upload deploy artifact to particular Bitbucket.org repository - Downloads secion
* Enables particular downloads section of one Bitbucker.org repository as a Maven Repository
* Enables all Download sections of repositories of on owner (team or user) to be used as a one Maven Repository

# Basic configuration

## settings.xml configuration
As for each protected maven repository you should register credentials for it:

```xml
<server>
	<id>bitbucket</id>
	<username>${owner-username-in-bitbucket}</username>
	<password>${your-bitbucket-password}</password>
</server>
```

**owner-username-in-bitbucket** should be your usernamer or team username
**your-bitbucket-password** should be your password or an **app password** created as stated [here](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) and [there](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html).

If would like to use bitbucket as a dependency source for your projects register the repository:

```xml
<profile>
	<id>craftware</id>
	<repositories>
		<repository>
			<id>bitbucket</id>
			<url>bitbucket://${owner-username-in-bitbucket}/${optional-repository-slug}</url>
			<snapshots>
				<enabled>...</enabled>
			</snapshots>
			<releases>
				<enabled>...</enabled>
			</releases>
		</repository>
	</repositories>			
</profile>
```

**owner-username-in-bitbucket** should correspond to credentials created  before.
**optional-repository-slug** should be a valid slug of a repository upon which maven should search dependencies. This can be empty. About the repository slug you can read [here](https://confluence.atlassian.com/bitbucket/what-is-a-slug-224395839.html).

You can also register plugin repository as follows:

```xml
<pluginRepositories>
	<pluginRepository>
	<id>bitbucket</id>
	<url>bitbucket://${owner-username-in-bitbucket}/${optional-repository-slug}</url>
	<releases>
		<enabled>...</enabled>
	</releases>
	<snapshots>
		<enabled>...</enabled>
	</snapshots>
	</pluginRepository>
</pluginRepositories>
```

## pom.xml configuration

**The most important** configurations must be placed in your pom.xml. Following snippet registers extension for maven wagon transport layer.

```xml
<build>
	<extensions>
		<extension>
			<groupId>org.codedoers.maven</groupId>
			<artifactId>bitbucket-download-wagon</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</extension>
	</extensions>
</build>
```
**Please note** that the version available in maven central or jcenter may be newer than in example snippet.

You can register bitbucket as a distribution management endpoint:

```xml
<distributionManagement>
	<repository>
		<id>bitbucket</id>
		<name>bitbucket</name>
		<url>bitbucket://${owner-username-in-bitbucket}/${repository-slug}</url>
	</repository>
</distributionManagement>
```
**owner-username-in-bitbucket** should correspond to credentials created  before.
**repository-slug** should be a valid slug of a repository upon which maven should upload/deploy your released distribution/artifacts. **This can NOT be empty** - this slug points to the specific repository you want to have released files.


# Use cases

## Uploading a release or other files with maven deploy

If you want to deploy your distribution to bitbucket:

1. Place you credentials in settings.xml - this can be your username and password or teamname and app-password.
2. Register extension in your pom.xml.
3. Register distribution management with proper url. **owner-username-in-bitbucket**.
4. Use maven release-plugin or jgitflow or deploy to release your files. 

## Downloading dependencies from specific repository downloads

1. Place you credentials in settings.xml - this can be your username and password or teamname and app-password.
2. Register extension in your pom.xml.
3. Register repository in settings.xml. Use a specific repository SLUG in url - in this scenario repository slug should not be optional.


## Downloading dependencies from all team or user repositories

1. Place you credentials in settings.xml - this can be your username and password or teamname and app-password.
2. Register extension in your pom.xml.
3. Register repository in settings.xml. Don't place a repository slug in url. When searching for dependencies - maven will look to all repositories to all download sections for the specific owner.

## Using as a holder area for your project dependencies

1. Place you credentials in settings.xml - this can be your username and password or teamname and app-password.
2. Register extension in your pom.xml.
3. Copy or deploy all dependencies required to your project to downloads section.
3. Register repository in settings.xml. Use a specific repository SLUG in url - in this scenario repository slug should not be optional.
