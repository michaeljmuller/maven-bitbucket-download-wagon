/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.maven.wagon.AbstractWagon;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.codedoers.maven.bitbucket.BitbucketURLFactory;
import org.codedoers.maven.bitbucket.DownloadItem;
import org.codedoers.maven.bitbucket.Repository;
import org.codedoers.maven.bitbucket.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitbucketDownloadWagon extends AbstractWagon {

	private static final Logger log = LoggerFactory.getLogger(BitbucketDownloadWagon.class);

	private BitbucketCaller bitbucket;
	private BitbucketURLFactory urlFactory;
	private Collection<Repository> repositories;

	@Override
	protected void openConnectionInternal() throws ConnectionException, AuthenticationException {
		this.urlFactory = new BitbucketURLFactory(getRepository().getUrl());
		this.bitbucket = new BitbucketCaller(
				  getAuthenticationInfo().getUserName(),
				  getAuthenticationInfo().getPassword());
		if (!urlFactory.withRepository()) {
			fetchRepositories();
		}
	}

	@Override
	public void closeConnection() throws ConnectionException {
		log.info("Closing bitbucket repositories, nothing to do here...");
	}

	private void fetchRepositories() throws ConnectionException {
		log.info("Fetching repositories for - {}", getRepository().getUrl());
		repositories = bitbucket.page(urlFactory.repositories())
				  .as(Repository.Page.class)
				  .iterator(bitbucket)
				  .stream()
				  .collect(Collectors.toList());
	}

	@Override
	public void get(String filename, File file)
			  throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
		log.info("Fetching {} to file {}", filename, file.getPath());
		String name = normalizeFileName(filename);
		filterStreamAndDownload(streamDownloadItems(), name, file);
	}

	private Stream<DownloadItem> streamDownloadItems() {
		if (urlFactory.withRepository()) {
			return streamFromDefaultRepository();
		}
		return streamFromAllRepositories();
	}

	private Stream<DownloadItem> streamFromAllRepositories() {
		return repositories.stream()
				  .map(Repository::getDownloads)
				  .map(Self::getHref)
				  .map(bitbucket::page)
				  .map(c -> c.as(DownloadItem.Page.class))
				  .map(i -> i.iterator(bitbucket))
				  .flatMap(PageableIterator::stream);
	}

	private Stream<DownloadItem> streamFromDefaultRepository() {
		return bitbucket.page(urlFactory.downloads()).as(DownloadItem.Page.class).iterator(bitbucket).stream();
	}

	private void filterStreamAndDownload(Stream<DownloadItem> stream, String name, File file)
			  throws TransferFailedException, ResourceDoesNotExistException {
		Optional<String> found = stream
				  .filter(di -> name.equals(di.getName()))
				  .map(DownloadItem::getLinks)
				  .map(DownloadItem.Links::getSelf)
				  .map(Self::getHref)
				  .findAny();
		try {
			bitbucket.download(file, found.orElseThrow(() -> new ResourceDoesNotExistException(name + " not found")));
		} catch (IOException ex) {
			throw new TransferFailedException("Bitbucket error", ex);
		}
	}

	@Override
	public boolean getIfNewer(String filename, File file, long l)
			  throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
		get(filename, file);
		return true;
	}

	@Override
	public void put(File file, String filename)
			  throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
		log.info("Deploying {} as {}", file.getAbsolutePath(), filename);
		try {
			bitbucket.upload(file, urlFactory.upload(), normalizeFileName(filename));
		} catch (IOException ex) {
			throw new TransferFailedException("Bitbucket error", ex);
		}
	}

	private String normalizeFileName(String name) {
		return name.replace("/", "-");
	}

}
